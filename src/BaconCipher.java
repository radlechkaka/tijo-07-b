import java.util.HashMap;
import java.util.Map;

public class BaconCipher implements Cipher {
 private static final Map<Character, String> encodingMap = new HashMap<Character, String>();
    static {
        encodingMap.put('A',"aaaaa");
        encodingMap.put('B',"aaaab");
        encodingMap.put('C',"aaaba");
        encodingMap.put('D',"aaabb");
        encodingMap.put('E',"aabaa");
        encodingMap.put('F',"aabab");
        encodingMap.put('G',"aabba");
        encodingMap.put('H',"aabbb");
        encodingMap.put('I',"abaaa");
        encodingMap.put('J',"abaaa");
        encodingMap.put('K',"abaab");
        encodingMap.put('L',"ababa");
        encodingMap.put('M',"ababb");
        encodingMap.put('N',"abbaa");
        encodingMap.put('O',"abbab");
        encodingMap.put('P',"abbba");
        encodingMap.put('Q',"abbbb");
        encodingMap.put('R',"baaaa");
        encodingMap.put('S',"baaab");
        encodingMap.put('T',"baaba");
        encodingMap.put('U',"baabb");
        encodingMap.put('V',"baabb");
        encodingMap.put('W',"babaa");
        encodingMap.put('X',"babab");
        encodingMap.put('Y',"babba");
        encodingMap.put('Z',"babbb");
        encodingMap.put(' '," ");
    }

    private static final Map<String, Character> decodingMap = new HashMap<String, Character>();
    static {
        decodingMap.put("aaaaa",'A');
        decodingMap.put("aaaab",'B');
        decodingMap.put("aaaba",'C');
        decodingMap.put("aaabb",'D');
        decodingMap.put("aabaa",'E');
        decodingMap.put("aabab",'F');
        decodingMap.put("aabba",'G');
        decodingMap.put("aabbb",'H');
        decodingMap.put("abaaa",'I');
        decodingMap.put("abaab",'K');
        decodingMap.put("ababa",'L');
        decodingMap.put("ababb",'M');
        decodingMap.put("abbaa",'N');
        decodingMap.put("abbab",'O');
        decodingMap.put("abbba",'P');
        decodingMap.put("abbbb",'Q');
        decodingMap.put("baaaa",'R');
        decodingMap.put("baaab",'S');
        decodingMap.put("baaba",'T');
        decodingMap.put("baabb",'U');
        decodingMap.put("babaa",'W');
        decodingMap.put("babab",'X');
        decodingMap.put("babba",'Y');
        decodingMap.put("babbb",'Z');
    }

    @Override
    public String decode(final String message) {
        String decoded = "";
        int a = 0;
        int b = 5;
        String s = "";
        while(b <= message.length()) {
            s = message.substring(a,b);
            if(!s.contains(" ")) {
                decoded += decodingMap.get(s);
                a += 5;
                b += 5;
            } else {
                decoded += " ";
                a += 1;
                b += 1;
            }
        }
        return decoded;
    }

    @Override
    public String encode(final String message) {
        String encoded = "";
        for(Character c : message.toCharArray()) {
            encoded += encodingMap.get(c);
        }
        return encoded;
    }

}