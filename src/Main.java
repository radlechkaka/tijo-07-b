public class Main {
    public static void main(String[] args) {

        Cipher baconCipher = new BaconCipher();

        // Encode: baaabbabbbbabbaaababbaaaa aaaabaaaaaaaabaabbababbaaaaaaa
        System.out.println("Encode: " +
                baconCipher.encode("SZYFR BACONA"));

        // Decode: SZYFR BACONA
        System.out.println("Decode: " +
                baconCipher.decode("baaabbabbbbabbaaababbaaaa aaaabaaaaaaaabaabbababbaaaaaaa"));
    }
}
